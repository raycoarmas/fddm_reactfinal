import React from "react";
import { View, ScrollView, AsyncStorage } from "react-native";
import { Text, Button, List, ListItem, SearchBar } from "react-native-elements";

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      background: 'white',
      color: 'black',
      opacity: 0.5,
      data: [],
      completo: []
    };
    this.props.navigation.setParams({ 'background': 'white' })
    this.props.navigation.setParams({ 'color': 'black' })
    AsyncStorage.getItem('Style').then(this.searchStyle)
    AsyncStorage.getAllKeys().then(this.loadData)
  }

  searchStyle = value => {
    value = JSON.parse(value)
    if (value != null) {
      this.props.navigation.setParams({ 'background': value.background })
      this.props.navigation.setParams({ 'color': value.color })
      this.setState({
        background: value.background,
        color: value.color
      });
    }
    this.props.navigation.setParams({ 'reload': false })
  };

  search = (text) => {
    var data = this.state.completo.filter(item => {
      return item.title.toLowerCase().indexOf(text.toLowerCase()) > -1
    })
    var opacity = 0.5
    if(text.length > 0 ){
      opacity = 0.9
    }
    this.setState({
      data: data,
      opacity: opacity
    })
  }

  loadData = (value) => {
    AsyncStorage.multiGet(value, (err, stores) => {
      var resultFinal = []
      stores.map((result, i, store) => {
        if(store[i][0] != 'Style'){
          resultFinal[i] = JSON.parse(store[i][1])
          resultFinal[i].key = store[i][0]
        }
      });
      this.setState({
        data: resultFinal,
        completo: resultFinal
      })
    });
    this.props.navigation.setParams({ 'reload': false })
  }

  render() {
    const { navigation } = this.props;
    if (navigation.getParam('reload', false)) {
      AsyncStorage.getItem('Style').then(this.searchStyle)
      AsyncStorage.getAllKeys().then(this.loadData)
    }

    return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: this.state.background }}>
        <View style={{ flex: 0.9 }}>
          <Text h3 style={{ color: this.state.color}}>Mi Lista</Text>
          <SearchBar
            containerStyle={{ backgroundColor: this.state.background }}
            inputStyle={{backgroundColor: this.state.background, opacity: this.state.opacity}}
            placeholderTextColor= {this.state.color}
            onChangeText={this.search}
            placeholder='Buscar un Item' />
          <ScrollView >
            <List containerStyle={{ marginBottom: 20, backgroundColor: this.state.background }}>
              {
                this.state.data.map((dato) => (
                  <ListItem titleStyle={{ color: this.state.color }}
                    key={dato.key}
                    title={dato.title}
                    onPress={() => navigation.navigate('Detail',
                      {
                        title: dato.title,
                        body: dato.body,
                        key: dato.key
                      })}
                  />
                ))
              }
            </List>
          </ScrollView>
        </View>
        <View style={{ flex: 0.1, flexDirection: "row", alignItems: 'flex-end', marginBottom: 2 }}>
          <Button buttonStyle={{ backgroundColor: this.state.background }} textStyle={{ color: this.state.color }}
            title="Settings" onPress={() => navigation.navigate("Settings")} />
          <Text style={{ color: this.state.color }}>{this.state.data.length} Items</Text>
          <Button buttonStyle={{ backgroundColor: this.state.background }} textStyle={{ color: this.state.color }}
            title="Añadir" onPress={() => navigation.navigate("Editor")} />
        </View>
      </View>
    );
  }
}

export default HomeScreen;
