import React from "react";
import { View, ScrollView, AsyncStorage, Alert } from "react-native";
import { List, SearchBar, Button, CheckBox } from "react-native-elements";

class BorrarScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      background: 'white',
      color: 'black',
      opacity: 0.5,
      data: [],
      completo: [],
      buttonDelete: "Eliminar todo"
    };
    this.props.navigation.setParams({ 'background': 'white' })
    this.props.navigation.setParams({ 'color': 'black' })
    this.searchStyle
    AsyncStorage.getAllKeys().then(this.loadData)
  }

  searchStyle = AsyncStorage.getItem('Style').then(value => {
    value = JSON.parse(value)
    if (value != null) {
      this.props.navigation.setParams({ 'background': value.background })
      this.props.navigation.setParams({ 'color': value.color })
      this.setState({
        background: value.background,
        color: value.color
      });
    }
  });

  search = (text) => {
    var data = this.state.completo.filter(item => {
      return item.title.toLowerCase().indexOf(text.toLowerCase()) > -1
    })
    var opacity = 0.5
    if (text.length > 0) {
      opacity = 0.9
    }
    this.setState({
      data: data,
      opacity: opacity
    })
  }

  loadData = (value) => {
      AsyncStorage.multiGet(value, (err, stores) => {
        var resultFinal = []
        stores.map((result, i, store) => {
          if(store[i][0] != 'Style'){
            resultFinal[i] = JSON.parse(store[i][1])
            resultFinal[i].checked = false;
            resultFinal[i].key = store[i][0]
          }
        });
        this.setState({
          data: resultFinal,
          completo: resultFinal
        })
      });
      this.props.navigation.setParams({ 'reload': false })
  }

  press(dato) {
    var titles = this.props.navigation.getParam('titles', []);
    this.state.data.filter(item => {
      if (item.key == dato.key) {
        item.checked = !item.checked
        if (titles.indexOf(dato.key) > -1) {
          titles.splice(titles.indexOf(dato.key), 1);
        } else {
          titles[titles.length] = dato.key
        }
      }
    })
    this.props.navigation.setParams({ 'titles': titles })
    if (titles.length > 0) {
      this.setState({ buttonDelete: "Eliminar" })
    } else {
      this.setState({ buttonDelete: "Eliminar todo" })
    }
  }

  deleteAll = () => {
    AsyncStorage.getAllKeys().then((value) => {
      AsyncStorage.multiRemove(value).then(() => {
        this.setState({
          data: [],
          completo: []
        })
      })
    })
  }

  delete = () => {
    var titles = this.props.navigation.getParam('titles', []);
    if (titles.length > 1) {
      AsyncStorage.multiRemove(this.props.navigation.state.params.titles).then(
        this.props.navigation.navigate("Home", { reload: true })
      )
    } else {
      Alert.alert(
        'Alert',
        '¿Seguro que quiere borrarlo todo?',
        [
          { text: 'No', style: 'cancel' },
          { text: 'Yes', onPress: this.deleteAll }
        ],
        { cancelable: false }
      )
    }
  }

  render() {
    this.searchStyle
    return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: this.state.background }}>
        <View style={{ flex: 0.9 }}>
          <SearchBar
            containerStyle={{ backgroundColor: this.state.background }}
            inputStyle={{ backgroundColor: this.state.background, opacity: this.state.opacity }}
            placeholderTextColor={this.state.color}
            onChangeText={this.search}
            placeholder='Buscar un Item' />
          <ScrollView>
            <List containerStyle={{ marginBottom: 20, backgroundColor: this.state.background }}>
              {
                this.state.data.map((dato) => (
                  <CheckBox textStyle={{ color: this.state.color }} containerStyle={{ backgroundColor: this.state.background }}
                    title={dato.title}
                    onPress={() => this.press(dato)}
                    checked={dato.checked}
                    key={dato.key}
                  />
                ))
              }
            </List>
          </ScrollView>
        </View>
        <View style={{ flex: 0.1, flexDirection: "row", alignItems: 'flex-end', marginBottom: 2 }}>
          <Button buttonStyle={{ backgroundColor: this.state.background }} textStyle={{ color: this.state.color }}
            title={this.state.buttonDelete} onPress={this.delete} />
        </View>
      </View>
    );
  }
}

export default BorrarScreen;
