import React from "react";
import { View, AsyncStorage } from "react-native";
import { Text } from "react-native-elements";

class DetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      background: 'white',
      color: 'black',
    };
    this.props.navigation.setParams({ 'background': 'white' })
    this.props.navigation.setParams({ 'color': 'black' })
    this.searchStyle
  }

  searchStyle = AsyncStorage.getItem('Style').then(value => {
    value = JSON.parse(value)
    if (value != null) {
      this.props.navigation.setParams({ 'background': value.background })
      this.props.navigation.setParams({ 'color': value.color })
      this.setState({
        background: value.background,
        color: value.color
      });
    }
  });

  render() {
    const title = this.props.navigation.getParam('title','No title')
    const body = this.props.navigation.getParam('body','No Body')
    this.searchStyle
    return (
      <View style={{ flex: 1, backgroundColor: this.state.background}}>
        <View style={{ flex: 1, marginLeft: 10}}>
          <Text h3 style={{ color: this.state.color }}>{title}</Text>
          <Text style={{ color: this.state.color }}>{body}</Text>
        </View>
      </View>
    );
  }
}

export default DetailScreen;
