import React from "react";
import { View, AsyncStorage } from "react-native";
import { FormLabel, FormInput } from "react-native-elements";

class EditorScreen extends React.Component {
  constructor(props) {
    super(props);
    const key = this.props.navigation.getParam('key', '')
    if (key != '') {
      this.props.navigation.setParams({ 'oldKey': key })
    } else {
      this.props.navigation.setParams({ 'key': new Date() })
    }
    this.state = {
      background: 'white',
      color: 'black',
      opacityTitle: 0.5,
      opacityBody: 0.5,
    };
    this.props.navigation.setParams({ 'background': 'white' })
    this.props.navigation.setParams({ 'color': 'black' })
    this.searchStyle
  }

  searchStyle = AsyncStorage.getItem('Style').then(value => {
    value = JSON.parse(value)
    if (value != null) {
      this.props.navigation.setParams({ 'background': value.background })
      this.props.navigation.setParams({ 'color': value.color })
      this.setState({
        background: value.background,
        color: value.color
      });
    }
  });

  render() {
    const title = this.props.navigation.getParam('title', '')
    const body = this.props.navigation.getParam('body', '')
    this.searchStyle
    return (
      <View style={{ flex: 1, backgroundColor: this.state.background }}>
        <FormLabel labelStyle={{ color: this.state.color }}>Título</FormLabel>
        <FormInput inputStyle={{ opacity: this.state.opacityTitle }}
          placeholderTextColor={this.state.color}
          value={title}
          placeholder='Título'
          onChangeText={title => {
            this.props.navigation.setParams({ 'title': title })
            var opacity = 0.5
            if (title.length > 0) {
              opacity = 0.9
            }
            this.setState({ opacityTitle: opacity })
          }} />
        <FormLabel labelStyle={{ color: this.state.color }}>Cuerpo</FormLabel>
        <FormInput inputStyle={{ opacity: this.state.opacityBody }}
          placeholderTextColor={this.state.color}
          placeholder='Cuerpo'
          value={body}
          multiline={true}
          onChangeText={body => {
            this.props.navigation.setParams({ 'body': body })
            var opacity = 0.5
            if (body.length > 0) {
              opacity = 0.9
            }
            this.setState({ opacityBody: opacity })
          }} />
      </View>
    );
  }
}

export default EditorScreen;
