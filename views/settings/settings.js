import React from "react";
import { View, ScrollView, AsyncStorage } from "react-native";
import { Text, List, CheckBox } from "react-native-elements";

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      background: 'white',
      color: 'black',
      themes: themes
    }
    this.props.navigation.setParams({ 'background': 'white' })
    this.props.navigation.setParams({ 'color': 'black' })
    AsyncStorage.getItem('Style').then(value => {
      value = JSON.parse(value)
      if (value != null) {
        this.state.themes.filter(item => {
          if (item.name == value.background) {
            item.checked = true
          } else {
            item.checked = false
          }
        })
        
        this.props.navigation.setParams({ 'background': value.background })
        this.props.navigation.setParams({ 'color': value.color })
        this.setState({
          background: value.background,
          color: value.color
        })

      }
    });
  }

  press(dato) {
    this.state.themes.filter(item => {
      if (item.name == dato.name) {
        item.checked = true
        this.props.navigation.setParams({ 'background': item.name })
        this.props.navigation.setParams({ 'color': item.color })
        this.setState({
          background: item.name,
          color: item.color
        })
      } else {
        item.checked = false
      }
    })
    this.forceUpdate()
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: this.state.background }}>
        <Text h3 style={{ color: this.state.color }}>Themes</Text>
        <ScrollView >
          <List containerStyle={{ marginBottom: 20, backgroundColor: this.state.background }}>
            {
              this.state.themes.map((theme) => (
                <CheckBox textStyle={{ color: this.state.color }} containerStyle={{ backgroundColor: this.state.background }}
                  title={theme.name}
                  onPress={() => this.press(theme)}
                  checked={theme.checked}
                  key={theme.name}
                />
              ))
            }
          </List>
        </ScrollView>
      </View>
    );
  }
}

const themes = [
  {
    name: 'black',
    checked: false,
    color: 'white'
  },
  {
    name: 'white',
    checked: true,
    color: 'black'
  }
]

export default Settings;