import React from "react";
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { AsyncStorage } from "react-native";
import { Button, Icon } from "react-native-elements";

import Settings from '../views/settings/settings';
import Home from "../views/home/home";
import Detail from "../views/home/detail";
import Editor from '../views/home/editor';
import Borrar from '../views/home/borrar';

const Stack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        title: "Home",
        headerBackTitle: "Home",
        headerRight: (<Button
          buttonStyle={{ backgroundColor: navigation.getParam('background', 'white') }} textStyle={{ color: navigation.getParam('color', 'black') }}
          title="Borrar" onPress={() => navigation.navigate("Borrar")} />),
        headerStyle: {
          backgroundColor: navigation.getParam('background', 'white')
        },
        headerTitleStyle: {
          color: navigation.getParam('color', 'black')
        }
      })
    },
    Detail: {
      screen: Detail,
      navigationOptions: ({ navigation }) => ({
        title: "Detail",
        headerLeft: (<Icon color={navigation.getParam('color', 'white')} containerStyle={{ marginLeft: 10 }} name="arrow-back" onPress={() => { navigation.navigate("Home", { reload: true }) }} />),
        headerRight: (<Button
          buttonStyle={{ backgroundColor: navigation.getParam('background', 'white') }} textStyle={{ color: navigation.getParam('color', 'black') }}
          title="Editar" onPress={() => navigation.navigate("Editor", {
            title: navigation.state.params.title,
            body: navigation.state.params.body,
            key: navigation.state.params.key,
          })} />),
        headerStyle: {
          backgroundColor: navigation.getParam('background', 'white')
        },
        headerTitleStyle: {
          color: navigation.getParam('color', 'black')
        }
      })
    },
    Editor: {
      screen: Editor,
      navigationOptions: ({ navigation }) => ({
        title: "Editor",
        headerRight: (<Button
          buttonStyle={{ backgroundColor: navigation.getParam('background', 'white') }} textStyle={{ color: navigation.getParam('color', 'black') }}
          title="OK" onPress={
            () => {
              if (navigation.getParam('title', '') == '') {
                navigation.navigate("Home");
              } else if (navigation.getParam('OldTitle', '') != '') {
                AsyncStorage.mergeItem(navigation.state.params.oldKey,
                  JSON.stringify({ title: c, body: navigation.state.params.body })).then(navigation.navigate('Home',{ reload: true }));
              } else {
                AsyncStorage.setItem(navigation.state.params.key,
                  JSON.stringify({ title: navigation.state.params.title, body: navigation.state.params.body })).then(navigation.navigate('Home', { reload: true }));
              }
            }
          } />),
        headerStyle: {
          backgroundColor: navigation.getParam('background', 'white')
        },
        headerTitleStyle: {
          color: navigation.getParam('color', 'black')
        },
        headerTintColor: navigation.getParam('color', 'white')
      })
    },
    Settings: {
      screen: Settings,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (<Icon color={navigation.getParam('color', 'white')} containerStyle={{ marginLeft: 10 }} name="arrow-back" onPress={() => {
            AsyncStorage.setItem('Style',JSON.stringify({ background: navigation.state.params.background, color: navigation.state.params.color })).then(
              navigation.navigate("Home", { reload: true }) 
            )
          }} />),
        headerStyle: {
          backgroundColor: navigation.getParam('background', 'white')
        }
      })
    },
    Borrar: {
      screen: Borrar,
      navigationOptions: ({ navigation }) => ({
        title: "Borrar",
        headerLeft: (null),
        headerRight: (<Button
          buttonStyle={{ backgroundColor: navigation.getParam('background', 'white') }} textStyle={{ color: navigation.getParam('color', 'black') }}
          title="Cancelar" onPress={() => {
            navigation.navigate("Home", { reload: true })
          }} />),
        headerStyle: {
          backgroundColor: navigation.getParam('background', 'white')
        },
        headerTitleStyle: {
          color: navigation.getParam('color', 'black')
        }
      })
    }
  },
  {
    initialRouteName: "Home"
  }
);


export const Container = createAppContainer(Stack)